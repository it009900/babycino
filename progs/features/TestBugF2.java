// Evaluate y even when x is true
class TestBugF2 {
    public static void main(String[] a) {
	if (new Test().f()) {} else {}
    }
}
class Test {
    public boolean f() {
    int n;
    int c;
    c = 0;
    n = 1;
	while (c < 5 || this.y(n)) {   // assume C < 5 for x
        System.out.println(n + 1);
        c = c + 1;
    }
	return false;
    }
    public boolean y(int n) {
        System.out.println(0);
        return false;
    }
}
//if this output 0 before 2, this means y does Evaluated even when x is true adn it import junit.framework.TestCase;
//output 2 five times this means x is true and the expression return true and
//it should print out 0 after five 2 printed becuase x retutrned false.
